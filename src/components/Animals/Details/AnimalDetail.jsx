import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import {FaBackward} from "react-icons/fa";
import { getAnimalsById } from '../../../services/servicesAnimals/apiAnimals';

const AnimalDetails = () => {
    const [animal,setAnimal] = useState({});
    const [family,setFamily] = useState({});
    const {id} = useParams();

    //SERVICES API
    const getAnimal = async () =>{
        const animalById = await getAnimalsById(id);
        setAnimal(animalById.data);
        setFamily(animalById.data.family)
        console.log(animalById.data.family)
    }

    useEffect(() =>{
        getAnimal();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    },[])
    return (
        <>
        <Link to="/animals"><FaBackward/></Link>
        <div className="animals">
        
            <div className="animals__details">
                <h3>{animal.name}</h3>
                <p>{animal.carnivore ? 'Si' : 'No'} es carnivoro.</p>
                <p>Es de la familia {family.species}</p> 
                <p>{family.livingInGroup ? 'Si' : 'No'} viven en grupo.</p> 
            </div>
        </div>
        </>
    )
};

export default AnimalDetails;