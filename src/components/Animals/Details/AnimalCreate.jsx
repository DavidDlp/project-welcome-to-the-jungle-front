import React from 'react';
import { Link } from 'react-router-dom';
import {FaBackward} from "react-icons/fa";

const AnimalCreate = () =>{
    return (
        <>
        <Link to="/animals"><FaBackward/></Link>
            <div>
                <h3>Esto es el AnimalCreate</h3>
            </div>
        </>
    )
};

export default AnimalCreate;