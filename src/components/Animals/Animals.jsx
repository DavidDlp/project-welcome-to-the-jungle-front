import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router";
import { Link } from 'react-router-dom';
import { AiFillDelete } from "react-icons/ai";
import { MdUpdate} from "react-icons/md";
import { deletedAnimal, getAllAnimals, updatedAnimal } from '../../services/servicesAnimals/apiAnimals';

const Animals = () =>{
    const [animals, setAnimals] = useState([]);
    const [searchAnimal, setSearchAnimal] = useState("");
    const [finallyAnimal, setFinallyAnimal] = useState([]);

    const navigate = useNavigate();

    //SERVICES API
    const getAnimalsList = async () =>{
        try{
            const result = await getAllAnimals();
            setAnimals(result.data);
            setFinallyAnimal(result.data);
            // console.log(result.data);
        }catch(error){
            return console.error(error);
        }
    };
    const deleteAnimal = async (id)=> {
        try {
            await deletedAnimal(id)
            const newElements = animals.filter(item=>item._id !== id)
            setAnimals(newElements)

        } catch (error) {
            console.error(error)
        }
    };

    const updateAnimal = async (id)=> {
        try {
            await updatedAnimal(id)
            const newElements = animals.filter(item=>item._id !== id)
            setAnimals(newElements)

        } catch (error) {
            console.error(error)
        }
    };

    //BUSCADOR
    const handleChange = (e) => {
        setSearchAnimal(e.target.value);
        // console.log("search: " + searchAnimal)
        filterSearch(e.target.value);
    };

    const filterSearch = (paramsSearch) =>{
        // eslint-disable-next-line array-callback-return
        let resultSearch = finallyAnimal.filter((element) =>{
            if(
                element.name
                    .toString()
                    .toLowerCase()
                    .includes(paramsSearch.toLowerCase())
            ){
                return element;
            }
        })
        setAnimals(resultSearch);
    };

    useEffect(() =>{
        getAnimalsList()
    },[])

    return (
        <>
        <div className="animals">
            <div className="animals__search">
                <input
                    value={searchAnimal}
                    type="text"
                    placeholder="Buscar nombre"
                    onChange={handleChange}
                />
            </div>
            <div>
                <h3>Welcome to the jungle </h3>
                <ul className="animals__list">
                    {animals.map((item) =>{
                        return (
                            <div className="animals__list--content">
                                <div className="animals__list--content--item">
                                    <Link to={{pathname: "/animals/details/" + item._id }} >
                                        <li key={JSON.stringify(item)}>{item.name}</li>
                                    </Link>
                                </div>
                                <div className="animals__list--content--button">
                                    <button onClick={()=>deleteAnimal(item._id)}><AiFillDelete/></button>
                                    <button onClick={()=>updateAnimal(item._id)}><MdUpdate/></button>
                                </div>
                            </div>
                        )
                    })}

                </ul>
            </div>
            <button className="animals__btn-create" onClick={() => navigate("/animals/create")}>New Animal</button>
        </div>
        </>
    )
};

export default Animals;