import React from 'react';
import './styles/App.scss';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./components/Home/Home";
import Habitat from "./components/Habitat/Habitat";
import Animals from "./components/Animals/Animals";
import NavBar from './components/NavBar/Navbar';
import AnimalDetails from './components/Animals/Details/AnimalDetail';
import AnimalCreate from './components/Animals/Details/AnimalCreate';

function App() {
  return (
    <div>
      <BrowserRouter>
      <header>
        <NavBar/>
      </header>
        <Routes>
          <Route path="/">
            <Route index element = {<Home/>} />
            <Route path="habitat" element= {<Habitat/>} />
            <Route path="animals" element= {<Animals/>} />
            <Route path="animals/create" element= {<AnimalCreate/>} />
            <Route path="animals/details/:id" element={<AnimalDetails/>}/>
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
