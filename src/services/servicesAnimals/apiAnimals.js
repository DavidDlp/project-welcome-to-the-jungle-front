import axios from 'axios';
import { ANIMALS_URL } from '../apiRoutes';

const config = {
    headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
    }
};

//GET
export const getAllAnimals = async () =>{
    try{    
        const req = await axios.get(ANIMALS_URL, config)
        return req
    }catch(error){
        return console.error(error)
    }
};

export const getAnimalsById = async (id) =>{
    try{
        const req = await axios.get(ANIMALS_URL +'/' +id,config)
        return req
    }catch(error){
        return console.error(error)
    }
};

//POST

//UPDATED
export const updatedAnimal = async (id) =>{
    try{
        const req = await axios.put(ANIMALS_URL +'/' +id,config)
        return req
    }catch(error){
        return console.error(error)
    }
}

//DELETE
export const deletedAnimal = async (id) =>{
    try{
        const req = await axios.delete(ANIMALS_URL +'/' +id,config)
        return req
    }catch(error){
        return console.error(error)
    }
}

