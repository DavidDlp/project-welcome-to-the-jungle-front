const BASE_URL = "http://localhost:4000";

const ANIMALS_URL = `${BASE_URL}/animals`;

export {
    BASE_URL,
    ANIMALS_URL
}